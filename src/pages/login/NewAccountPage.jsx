import React, { Component } from 'react';
import { IonPage, IonToolbar, IonContent, IonCardHeader, IonRow, IonCol, IonLabel, IonItem} from '@ionic/react';
import NewAccount from '../../components/newAccount/NewAccount';
import './NewAccountPage.css';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class NewAccountPage extends Component {
  componentDidMount(){
    if(this.props.user_state.login){
      return 
    }
  }

  _goBack = () => {
    return this.props.history.goBack();
  }
    render() {
      return (
        this.props.user_state.login ? <Redirect to="/home"/> : 
          <IonPage>
            <IonContent className="newAccount-background"> 
              <IonToolbar className="newAccount-toolbar">
                  <IonCardHeader className="newAccount-header"> ¡Registrate! </IonCardHeader>
              </IonToolbar>
                <NewAccount></NewAccount>
                <IonRow>
                  <IonCol>
                    <Link to={{pathname: "/login"}} style={{ textDecoration: 'none' }}>
                      <IonItem button className="links-items" lines="none">
                        <IonLabel color="white">¿Ya tienes una cuenta? ¡Inicia sesión!</IonLabel>
                      </IonItem>
                    </Link>
                  </IonCol>
                </IonRow>
              </IonContent>
          </IonPage>
      )
    }
}

const mapStateToProps = state => {
  return {
      user_state: state.user_state
  }
}

export default connect(mapStateToProps)(NewAccountPage);