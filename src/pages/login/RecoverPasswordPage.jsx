import React, { Component } from 'react';
import { IonPage, 
    IonContent,
    IonLabel } from '@ionic/react';
import RecoverPassword from '../../components/recoverPassword/RecoverPassword';
import HeaderComponent from '../../components/pageComponents/HeaderComponent'
//import o from '../../images/o.png';
import './RecoverPasswordPage.css'
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class RecoverPasswordPage extends Component {
    componentDidMount(){
        if(this.props.user_state.login){
            return 
        }
    }
    render() {
        return (
            this.props.user_state.login ? <Redirect to="/home"/> : 
            <IonPage>
                <IonContent className="recover-background"> 
                    <HeaderComponent
                        headerTitle="¿Olvidaste tu contraseña?" 
                        image={'assets/avatars/recover.png'}
                        message="No te preocupes... ¡Nosotros te ayudaremos a recuperarla!"/> 
                    <RecoverPassword></RecoverPassword>
                    <div style={{display: "inline-block", width: "50%"}}>
                        <Link to={{pathname: "/new"}} style={{ textDecoration: 'none' }}>
                        <div style={{textAlign: "center", display: "block" , position:"relative", top:"0px"}}>
                            <img alt='assets/avatars/recover.png' src = {'assets/avatars/register.png'} className="footer-image-recover"/>
                            <IonLabel color="white" style={{ fontFamily: "'Kalam', cursive"}}>¿Eres nuevo en la aplicación? ¡Registrate!</IonLabel>
                        </div>
                        </Link>
                    </div>

                    <div style={{display: "inline-block", width: "50%"}}>
                    <Link to={{pathname: "/login"}} style={{ textDecoration: 'none' }}>
                        <div style={{textAlign: "center", display: "block", position:"relative", top:"0px"}}>
                        <img alt='assets/avatars/register.png' src = {'assets/avatars/login_doggy.png'} className="footer-image-recover"/>
                        <IonLabel color="white" style={{ fontFamily: "'Kalam', cursive"}}>¿Ya eres parte? Inicia sesión</IonLabel>
                        </div>
                    </Link>
                    </div>
                </IonContent>     
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default connect(mapStateToProps)(RecoverPasswordPage);