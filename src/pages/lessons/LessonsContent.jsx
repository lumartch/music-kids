import React, { Component } from 'react';
import { IonPage, IonHeader,IonContent } from '@ionic/react';
import ContentHeader from '../../components/contentComponents/ContentHeader';
import ContentBody from '../../components/contentComponents/ContentBody';
import ContenLessons from '../../lessons-json/content-lessons.json';
import './LessonsContent.css';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import store from '../../redux/store';
import { updateProgress } from '../../redux/actions/ProgressActions';
import { setLesson } from './SetLessonConst';

class LessonsContent extends Component {
    constructor(){
        super()
        this.state = {
            lessonName: undefined,
            host: undefined,
            video: undefined,
            body: {},
            activity: undefined,
            header: undefined, 
            lesson: undefined
        }
    }

    componentDidMount() {
        if(!this.props.user_state.login){
            return 
        }
        if(this.props.location.state === undefined) {
            this._goBack()
        } else {
            this._updateHeaderLesson(this.props.location.state.header, this.props.location.state.lesson)
            Object.entries(ContenLessons).map( lessonsList => {
                if ( this.props.location.state.lesson === lessonsList[0]){
                    this.setState({
                        lessonName: lessonsList[0],
                        host: lessonsList[1]["host"],
                        video: lessonsList[1]["video"],
                        body:  lessonsList[1]["body"],
                        activity: lessonsList[1]["activity"]
                    })
                    this._updateJsonServer(lessonsList[1]["activity"], lessonsList[1]["body"]["field"], this.props.location.state.header, this.props.location.state.lesson)
                }
                return null
            } )
        }
    }

    componentDidUpdate(){
        if(!this.props.user_state.login) {
            return
        } 
        if(this.state.lesson === undefined){
            this._updateHeaderLesson(this.props.location.state.header, this.props.location.state.lesson)
        }
    }

    _updateHeaderLesson(header, lesson){
        this.setState({
            header: header,
            lesson: lesson,
        })
    }

    _updateJsonServer = async (activity, field, header, lesson) =>{ //Update cuando no hay actividad
        if(activity === false){
            let response = await setLesson(this.props.user_state.user["correo"], 1, field)
            if(response === "1"){
                store.dispatch(updateProgress(header, lesson))
            }
        }
    }

    _goBack = () => {
        return this.props.history.goBack();
    }

    render() {
        const { lessonName, video, host, body, activity } = this.state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> : 
                <IonPage>
                    <IonContent className="lesson-content">
                        <IonHeader className="ion-no-border">
                            <ContentHeader lessonName={ lessonName } goBack={this._goBack}/>
                        </IonHeader>
                        <ContentBody 
                            video={ video } 
                            host={ host } 
                            body={ body } 
                            activity={ activity }
                            lessonName={ lessonName }
                            header={ this.state.header }
                        />
                    </IonContent>
                </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default connect(mapStateToProps)(LessonsContent);