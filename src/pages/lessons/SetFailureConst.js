import { LINK_SERVER,IS_LOCAL } from "../../Const";

export const setFailure = (correo, value, field) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        var url = IS_LOCAL ? LINK_SERVER + "/ia/" : LINK_SERVER +  "/ia/setFailure.php";
        xhttp.open(IS_LOCAL ? "PUT" : "POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                resolve(xhttp.responseText)
            } else {
                resolve(xhttp.responseText);
            }
        };
        var json = JSON.stringify({
            "correo": correo,
            "value": parseInt(value), 
            "field": field
        });
        xhttp.send(json);
    })
}