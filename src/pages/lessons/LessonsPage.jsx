import React, { Component, } from 'react';
import { IonPage, 
    IonToolbar, 
    IonButtons, 
    IonMenuButton, 
    IonTitle, 
    IonContent } from '@ionic/react';
import LessonsList from '../../components/lessonComponents/LessonsList';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import "./LessonsPage.css";

class LessonsPage extends Component {
    componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
    }

    componentDidUpdate(){
        if(!this.props.user_state.login) {
            return
        } 
    }
    
    render() {
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> : 
            <IonPage>
                <IonContent className="lessons-list">
                    <IonToolbar className="lessons-toolbar">
                        <IonButtons>
                            <IonTitle className="title-font-lessons" color="white">Mis lecciones</IonTitle>
                            <IonMenuButton color="white" className="lessons-list-menu-button" autoHide={false}/>
                        </IonButtons>
                    </IonToolbar>
                    <LessonsList></LessonsList>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user_state: state.user_state
    }
}

export default connect(mapStateToProps)(LessonsPage);