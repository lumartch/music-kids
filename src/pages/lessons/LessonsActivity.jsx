import React, { Component } from 'react';
import { IonPage, 
    IonHeader,
    IonContent, 
    IonAlert, 
    IonCard, 
    IonCardContent, 
    IonProgressBar,
    IonIcon } from '@ionic/react';
import { star } from 'ionicons/icons'
import ContentHeader from '../../components/contentComponents/ContentHeader';
import ActivityContent from '../../components/activityComponents/ActivityContent';
import BasicPiano from '../../components/piano/BasicPiano';
import ActivityLessons from '../../lessons-json/activity-lessons';
import okayImage from "../../images/okay.png";
import wrongImage from "../../images/wrong.png";
import lessonFailed from "../../images/LessonFailed.png";
import lessonPassed from "../../images/LessonPassed.png";
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { updateProgress } from '../../redux/actions/ProgressActions';
import { updateLessonField } from '../../redux/actions/LessonsActions'
import store from '../../redux/store';
import './LessonsActivity.css';
import { setLesson } from './SetLessonConst';
import { setFailure } from './SetFailureConst';

var noteRange = {
    first: 48,
    last: 72,
}

class LessonsActivity extends Component {
    constructor(){
        super()
        this.state = {
            lessonName: undefined,
            header: undefined,
            lesson: undefined,
            correctAnswers: 0,
            renderActivity: true,
            song: [[]], 
            questionNumber: 0,
            showAlert: false,
            title: "",
            subtitle: "",
            message: "",
            func: undefined,
            field: undefined,
            unmountPiano: undefined,
            answerFlag: false
        }
    }

    _updateJsonServer = async(header, lesson) =>{
        if(header === undefined || lesson === undefined){
            return
        }
        let response = await setLesson(this.props.user_state.user["correo"], 1, this.state.field)
        if(response === "1"){
            store.dispatch(updateProgress(header, lesson))
        }
    }

    _updateFailureServer = async(header, lesson, value, finished) => {
        if(header === undefined || lesson === undefined){
            return
        }
        let response = await setFailure(this.props.user_state.user["correo"], value, this.state.field)
        if(response === "1"){
            store.dispatch(updateLessonField(header, lesson, finished))
        }
    }

    _generateHTML = (myText, myImage) =>{
        return (`<div> 
                    <label class="alert-label"> ${myText} </label> 
                    <img class="alert-img" alt="${myImage}" src=${myImage} /> 
                </div>`)
    }

    _generateAlertFinal = (myText, myImage) =>{
        return (`<div> 
                    <label class="alert-label-final"> ${myText} </label> 
                    <img class="alert-img-final" alt="${myImage}" src=${myImage} /> 
                </div>`)
    }

    _hideAlert = () => {
        this.setState( {
            showAlert: false
        })
    }

    componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
        
        this._updateHeaderLesson(this.props.location.state.header, this.props.location.state.lesson)
        if(this.props.location.state === undefined) {
            this._goBack()
        } else {
            Object.entries(ActivityLessons).map( activity => {
                if ( this.props.location.state.lesson === activity[0]) {
                    noteRange["first"] = activity[1]["First"]
                    noteRange["last"] = activity[1]["Last"]
                    this.setState({ 
                        lessonName: this.props.location.state.lesson, 
                        field:  activity[1]["field"],
                        func: this._hideAlert,
                        unmountPiano: false
                    })
                }
                return null
            })
        }
    }

    async componentWillUnmount(){
        this.setState({ unmountPiano: true, song: [] })
    }

    _updateHeaderLesson(header, lesson){
        if(!this.props.user_state.login){
            return 
        }
        this.setState({
            header: header,
            lesson: lesson,
        })
    }

    componentDidUpdate(){
        if(!this.props.user_state.login){
            return 
        }
        if(this.state.renderActivity === false){
            this.setState({
                renderActivity: true
            })
        }
        if(this.state.header === undefined && this.props.location.pathname === "/lessonsactivity"){
            this._updateHeaderLesson(this.props.location.state.header, this.props.location.state.lesson)
        }
    }

    _goBack = () => {
        this._handleSong([]);
        return this.props.history.goBack();
    }

    _handleAnswers = (answer) => {
        if(this.state.questionNumber === 4){
            document.getElementById(this.state.correctAnswers+1).style.color = "yellow"
            if(this.state.correctAnswers/3 >= 1){
                this.setState({
                    message: this._generateAlertFinal("¡Felicidades! Haz logrado pasar la lección!", lessonPassed),
                    correctAnswers: answer ? this.state.correctAnswers + 1 : this.state.correctAnswers,
                    song: [[]],
                    showAlert: true,
                    questionNumber: 5,
                    func: this._goBack,
                    answerFlag: answer
                })
                this._updateJsonServer(this.state.header, this.state.lesson);
                this._updateFailureServer(this.state.header, this.state.lesson, -1, true);
            } else {
                this.setState({
                    message: this._generateAlertFinal("¡UPS! Necesitas repasar el tema de nuevo.", lessonFailed),
                    song: [[]],
                    showAlert: true,
                    questionNumber: 5,
                    answerFlag: answer,
                    func: this._goBack
                })
                this._updateFailureServer(this.state.header, this.state.lesson, 1, false);
            }
        } else {
            if(answer){
                document.getElementById(this.state.correctAnswers+1).style.color = "yellow"
                let myMessage = this._generateHTML("¡Correcto!", okayImage)
                this.setState({ 
                    showAlert: true,
                    correctAnswers: this.state.correctAnswers + 1,
                    renderActivity: false,
                    song: [[]], 
                    questionNumber: this.state.questionNumber + 1,
                    answerFlag: answer,
                    message: myMessage
                })
            } else {
                let myMessage = this._generateHTML("¡Incorrecto!", wrongImage)
                this.setState({
                    showAlert: true,
                    renderActivity: false,
                    song: [[]], 
                    questionNumber: this.state.questionNumber + 1,
                    answerFlag: answer,
                    message: myMessage
                })
            }
        }
    }

    _handleSong = (song) => {
        this.setState({ 
            song: song
        })
    }

    _renderActivity = () =>{
        if(this.state.lessonName !== undefined){
            return (
                <ActivityContent 
                    lessonName={ this.state.lessonName } 
                    handleAnswers={ this._handleAnswers } 
                    handleSong={ this._handleSong }
                    handlePianoRange = { this._handlePianoRange }
                    disabled = { false }
                    />
            )
        }
    }

    _generateStars(){
        let stars = []
        for (let index = 1; index <=5; index++) {
            var pos = index * 18 + 5;
            stars.push(
                <IonIcon icon={star} 
                    id={index}
                    key={index}
                    className="progress-star"
                    style={{left: `calc(${pos}% - 12px)`}}>
                </IonIcon> 
            )
        }
        return stars; 
    }

    render() {
        const { lessonName, 
            renderActivity,
            showAlert,
            func,
            title, 
            subtitle,
            message, 
            song, 
            correctAnswers,
            unmountPiano,
            answerFlag } = this.state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> :
            <IonPage>
                <IonContent className="activity-content">
                    <IonAlert 
                        isOpen={ showAlert }
                        onDidDismiss={ func }
                        backdropDismiss = { false }
                        cssClass = { answerFlag ? "alert-activity" : "alert-activity-wrong alert-activity" }
                        header = { title }
                        subHeader = { subtitle }
                        message = { message }
                        buttons = { ["Ok"] }
                    />
                    <IonHeader className="ion-no-border">
                        <ContentHeader lessonName={ lessonName } goBack={ this._goBack }/>
                    </IonHeader>
                    <IonCard className="card-body-activity">
                        {
                            renderActivity
                            ? this._renderActivity()
                            : null 
                        }
                        {
                            !unmountPiano
                            ?   <IonCardContent IonCardContent className="activity-container-piano">
                                    <IonCardContent className="activity-progress-bar-container">
                                        {
                                            this._generateStars()
                                        }
                                        <IonProgressBar 
                                        className="activity-progress-bar"
                                        value={ correctAnswers/5 }/>
                                    </IonCardContent>
                                    <BasicPiano 
                                        isPlayback={ true } 
                                        song={ song }
                                        noteRange = { noteRange }
                                    />
                                </IonCardContent>
                            : null
                        }
                        
                    </IonCard>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default connect(mapStateToProps)(LessonsActivity);