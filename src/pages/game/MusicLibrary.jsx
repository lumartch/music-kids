import React, { Component } from 'react';
import { IonPage, 
    IonToolbar, 
    IonButtons, 
    IonMenuButton, 
    IonContent, 
    IonButton, 
    IonSegment,
    IonSegmentButton,
    IonIcon, IonCard,
    IonSearchbar } from '@ionic/react';
import { searchOutline, heart, musicalNotesSharp, arrowBackOutline, trophy } from 'ionicons/icons';
import SongsList from '../../components/songComponents/SongsList';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import "./MusicLibrary.css"
import { fetchSongs } from '../../components/songComponents/SongListConst';
import { loadSongs } from '../../redux/actions/SongsActions';
import store from '../../redux/store';

class MusicLibrary extends Component {
    constructor(){
        super()
        this.state = {
            tab: "all",
            search: "", 
            showInput: false
        }
    }

    async componentDidUpdate(){
        if(!this.props.user_state.login) {
            return
        }
        let songs_state = await fetchSongs(this.props.user_state.user["correo"], "")
        await store.dispatch(loadSongs(songs_state))
    }

    async componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
        let songs_state = await fetchSongs(this.props.user_state.user["correo"], "")
        await store.dispatch(loadSongs(songs_state))
    }

    _handleSegment = e =>{
        this.setState({
            tab: e.detail.value,
        })
    } 

    _handleSearch = () => {
        this.setState({ showInput: !this.state.showInput, search: ""  })
    }

    _handleSearchSong = (e) =>{
        this.setState({ search: e.target.value })
    }

    _handleSearchClear = () => {
        this.setState({ 
            search: ""
         })
    }

    _handleSubmitSearch = (e) =>{
        e.preventDefault()
        this.setState({
            showInput: false,
            tab: this.state.search,
        })
    }

    _handleChangeTab = (tab) => {
        this.setState({ tab: tab })
    }

    render() {
        const { tab, search, showInput } = this.state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> :
            <IonPage>
                <IonContent className="song-list">
                    <IonToolbar className="song-toolbar">
                        <IonSegment onIonChange={(e) => this._handleSegment(e) } value={ tab } disabled={ showInput }>
                            <IonSegmentButton value="all"><IonIcon icon={musicalNotesSharp} color="success" /></IonSegmentButton>
                            <IonSegmentButton value="favorites"><IonIcon icon={heart} color="danger"/></IonSegmentButton>
                            <IonSegmentButton value="finished"><IonIcon icon={trophy} color="success"/></IonSegmentButton>
                        </IonSegment>
                        <IonButtons slot="end">
                            <IonButton onClick={this._handleSearch}><IonIcon icon={searchOutline}/></IonButton>
                            <IonMenuButton className="song-list-menu-button" autoHide={false}/>
                        </IonButtons>
                    </IonToolbar>
                    {
                        showInput 
                        ?   <form className="search-wrapper" onSubmit={this._handleSubmitSearch}>
                                <IonButton className="back-search" onClick={() => this.setState({showInput: false})}>
                                    <IonIcon style={{color: "black"}} icon={arrowBackOutline}/>
                                </IonButton>
                                <IonSearchbar animated={true} className="input-search" placeholder="Buscar canción o artista..."
                                    value={search}
                                    onIonChange={ this._handleSearchSong }
                                    onIonClear={ this._handleSearchClear }/>
                            </form>
                        :   null
                    }
                    <IonCard className="songs" disabled={showInput}>
                        <SongsList tab={tab} search={search} func={ this._handleChangeTab }></SongsList>
                    </IonCard> 
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
}

export default connect(mapStateToProps)(MusicLibrary);