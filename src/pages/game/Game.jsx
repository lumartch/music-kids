import React, { Component } from 'react';
import { IonPage, 
    IonToolbar, 
    IonButton, 
    IonContent,
    IonProgressBar,
    IonAlert, 
    IonLabel,
    IonIcon} from '@ionic/react';
import * as MidiConvert from 'midiconvert';
import BasicPiano from "../../components/piano/BasicPiano";
import RectangleRoll from '../../components/pianoRoll/RectangleRoll';
import PianoInterface from '../../components/pianoRoll/PianoInterface';
import { chevronBackOutline } from'ionicons/icons';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import store from '../../redux/store';
import { updateFinishedSong } from '../../redux/actions/SongsActions';
import "./Game.css";
import okayImage from "../../images/okay.png";
import wrongImage from "../../images/wrong.png";
import { fetchFinishedSong } from './GameConst';

let notes = []
let semiquaverTime = 0
let round = 0
let noteCounter = 0
let queueNotes = []
let func = undefined

class Game extends Component {
    constructor(){
        super()
        this.state = {
            json: undefined,
            soundfont: undefined, 
            mapNotes: undefined,
            playSong: undefined,
            notes: undefined,
            time: 0, 
            sound: [[]],
            currentNote: undefined, 
            counter: 0,
            barValue: 0,
            showAlert: false,
            message: undefined,
            startCounter: 3,
            songName: undefined,
            songId: undefined,
            back: false,
            unmountPiano: undefined, 
            answerFlag: false
        }
    }


    componentDidUpdate(){
        if(!this.props.user_state.login) {
            return
        } 
    }

    async componentWillUnmount(){
        this.setState({ unmountPiano: true, mapNotes: [] })
    }
    
    async componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
        if(this.props.location.state === undefined) {
            this._goBack()
        } else {
            let midi = await fetch(this.props.location.state.path).then(response => {
                let array = response.arrayBuffer()
                return array;
            })
            /* First: 48 Last: 72 */
            let json = MidiConvert.parse(midi)
            console.log(json);
            this.setState({
                json: json,
                mapNotes: [],
                playSong: false,
                soundfont: this.props.location.state.soundfont,
                songName: this.props.location.state.songName,
                songId: this.props.location.state.songId,
                unmountPiano: false,
                answerFlag: false
            })
            this.interval = setInterval(this._startGame, 1300 )
        }
    }

    _startGame = () => {
        if(this.state.startCounter > 0){
            this.setState({
                startCounter: this.state.startCounter - 1
            })
        }
        else {
            clearInterval(this.interval)
            /*this.setState({
                playSong: true
            })*/
            this._gameplay()
        }
    }

    _appendNote = (note) => {
        this.setState({
            sound: note
        })
    }

    _gameplay = () => {
        clearInterval(this.interval)
        this.setState({ 
            time: 0,
            mapNotes: [], 
            playSong: false
        })
        this._game()
    }   

    _game = () => {
        notes = []
        queueNotes = []
        noteCounter = 0
        notes = this._jsonToDictionary(this.state.json["tracks"])
        round = 60/(Math.round(this.state.json["header"]["bpm"]) * 2)
        this.interval = setInterval(this._timer, round * 1000 )
        semiquaverTime = round / 2
        this.setState({
            playSong: true, 
            counter: 0
        })
    } 

    _timer = async() => {
        if(this.state.playSong && !this.state.disbled){
            if(Object.keys(notes).length === noteCounter) {
                if( this.state.counter/Object.keys(notes).length > 0.60){
                    let response = await fetchFinishedSong(this.props.user_state.user["correo"], this.state.songId)
                    if(response === "1"){
                        this.setState({
                            time: 0,
                            mapNotes: [],
                            barValue: 0,
                            counter: 0,
                            answerFlag: true,
                            showAlert: true,
                            message: this._generateHTML("¡Felicidades! Haz terminado la canción.", okayImage)
                        })
                        store.dispatch(updateFinishedSong(this.state.songId))
                    } else {
                        this.setState({
                            time: 0,
                            mapNotes: [],
                            barValue: 0,
                            counter: 0,
                            answerFlag: false,
                            showAlert: true,
                            message:this._generateHTML( "¡Bien hecho! [Clap clap]. Pero lamentamos informarte que necesitas conexión a internet para actualizar tu trofeo :c", wrongImage)
                        })
                    }
                    func = () => {
                        this.setState({ showAlert: false })
                        this._goBack()
                    }
                } else {
                    this.setState({
                        time: 0,
                        mapNotes: [],
                        barValue: 0,
                        counter: 0,
                        showAlert: true,
                        message: this._generateHTML("¡Pieza incompleta!", wrongImage),
                        answerFlag: false
                    })
                    func = () => {
                        this.setState({ showAlert: false })
                        this._goBack()
                    }
                }
                clearInterval(this.interval)
            } else {
                if(notes[this.state.time.toFixed(2).toString()] !== undefined){
                    let currentNote = notes[this.state.time.toFixed(2).toString()];
                    let array  = []
                    for (let index = 0; index < currentNote.length; index++) {
                        let midiNumber = currentNote[index]["midi"]
                        let octave = Math.trunc(midiNumber/12) - 4
                        let indexNote = midiNumber%12
                        let semiquavers = Math.ceil(currentNote[index]["duration"] / semiquaverTime)
                        array.push(<RectangleRoll 
                            key={ this.state.time + index } 
                            semiquavers={ semiquavers }
                            indexNote={indexNote} 
                            octave={octave}
                            intervalTime={ round }
                            k = {this.state.time.toFixed(2).toString()}
                            midiNumber = {midiNumber}
                            noteKey = { this._noteKey }
                            disbled= { this.state.back }
                            />);
                    }
                    this.setState( { mapNotes: [ ...this.state.mapNotes, array ] } )
                }
            }
        }
        this.setState({ time: this.state.time + round })
    }

    _jsonToDictionary = (json) => {
        let dictionary = []
        Object.entries(json).map( (track) => {
            if(track[1]["notes"].length !== 0){
                Object.entries(track[1]["notes"]).map( (note) => {
                    if( dictionary[note[1]["time"].toFixed(2)] === undefined) {
                        dictionary[note[1]["time"].toFixed(2)] = [note[1]]
                    } else {
                        dictionary[note[1]["time"].toFixed(2)] = [...dictionary[note[1]["time"].toFixed(2)], note[1]]
                    }
                    return null
                })
            }
            return null 
        })
        return dictionary
    }

    _noteClicked = (note) => {
        if(queueNotes !== []) {
            if(queueNotes[0] === note){
                this.setState({
                    counter: this.state.counter + 1 
                })
                queueNotes.splice(0,1)
                console.log("Correcto")
            }
        }
    }

    _noteKey = (key) => {
        if(key.flag === "start"){
            queueNotes.push(notes[key.time.toString()][0]["midi"])
        } else {
            if(queueNotes.findIndex(item => item === notes[key.time.toString()][0]["midi"]) !== -1){
                queueNotes.splice(queueNotes.findIndex(item => item === notes[key.time.toString()][0]["midi"]), 1)
            }
            noteCounter++
            this.setState({
                barValue: noteCounter/Object.keys(notes).length
            }) 
        }
    }

    _grid = () => {
        if(this.state.playSong){
            return this.state.mapNotes.map((item, id) => {
                return item
            })
        } else {
            return null
        }   
    }

    _goBack = () => {
        this.setState({ mapNotes: [], back: true })
        clearInterval(this.interval)
        return this.props.history.goBack();
    }

    _generateHTML = (myText, myImage) =>{
        let myStr = this.state.counter/Object.keys(notes).length > 0.60 ? 'alert-label-ok' : 'alert-label-wrong' 
        return (`<div> 
                    <label class="${myStr}"> ${myText} </label> 
                    <img class="alert-img-game" alt="${myImage}" src=${myImage} /> 
                </div>`)
    }

    render() {
        const { soundfont, sound, showAlert, message, songName, answerFlag } = this.state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> :
            <IonPage>
                <IonContent className="game-page">
                    <IonAlert
                        isOpen={ showAlert }
                        onDidDismiss={ func }
                        backdropDismiss = { false }
                        cssClass =  {  answerFlag ? 'alert-game' : 'alert-game-wrong alert-game' } 
                        message = { message }
                        buttons = { ["Ok"] }
                    />
                    <IonContent className="content">
                        <IonToolbar className="game-toolbar">
                            <IonButton onClick={ this._goBack } className="back-game">
                                <IonIcon icon={ chevronBackOutline } color="white"/>
                            </IonButton>
                            <h1 color="white" className="game-title">{songName}</h1>
                        </IonToolbar>       
                        <IonProgressBar value={ this.state.barValue } ></IonProgressBar>
                        {
                            !this.state.playSong
                            ?   <div className="initial-counter"><IonLabel>
                                    { 
                                        this.state.startCounter === 0
                                        ? "¡A Jugar!"
                                        : this.state.startCounter
                                    }
                                    </IonLabel></div>
                            :   null
                        }
                        <IonContent className="notes">
                            {
                                this._grid()
                            }
                            <div className="div-line"/>
                            <PianoInterface appendNote={ this._appendNote } noteClicked={ this._noteClicked } disabled={ !this.state.playSong }/>
                            {
                                !this.state.unmountPiano
                                ?   <IonContent className="piano-soundprovider">
                                        <BasicPiano 
                                            isPlayback={ true } 
                                            instrumentName={ soundfont }
                                            song={ sound }
                                            isPianoHero={ true }
                                        />
                                    </IonContent> 
                                : null
                            }
                            
                        </IonContent>
                    </IonContent>
                </IonContent>
            </IonPage>
        );
    }
}

Game.defaultProps = {
    path : 'assets/music_resources/midi/Lost_Woods.mid',
    soundfont: "acoustic_grand_piano",
    songname: "Nombre de canción"
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default connect(mapStateToProps)(Game);