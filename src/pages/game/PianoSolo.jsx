import { IonButton, IonContent, IonIcon, IonPage, IonSelect, IonSelectOption, IonToolbar } from '@ionic/react';
import { chevronBackOutline } from 'ionicons/icons';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import BasicPiano from '../../components/piano/BasicPiano';
import "./Game.css";

class PianoSolo extends Component {
    constructor(){
        super()
        this.state = {
            soundfont: "acoustic_grand_piano"
        }
    }

    _goBack = () => {
        return this.props.history.goBack();
    }

    _changeSoundfont = (e) => {
        this.setState( {
            soundfont: e.detail.value
        } )
    }

    render() {
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> :
            <IonPage>
                <IonContent className="game-page">
                    <IonContent className="content">
                        <IonToolbar className="game-toolbar">
                            <IonButton onClick={ this._goBack } className="back-game">
                                <IonIcon icon={ chevronBackOutline } color="white"/>
                            </IonButton>
                            <h1 color="white" className="game-title">¡Juego libre!</h1>
                        </IonToolbar>
                        <h1 className="piano-solo-title">Selecciona el instrumento que mas te guste.</h1>
                        <IonButton className="piano-solo-soundfont">
                            <IonSelect value={ this.state.soundfont } onIonChange={ this._changeSoundfont }>
                                <IonSelectOption value="acoustic_grand_piano">Piano</IonSelectOption>
                                <IonSelectOption value="acoustic_guitar_steel">Guitarra</IonSelectOption>
                                <IonSelectOption value="flute">Flauta</IonSelectOption>
                                <IonSelectOption value="violin">Violin</IonSelectOption>
                                <IonSelectOption value="church_organ">Orgáno</IonSelectOption>
                                <IonSelectOption value="accordion">Acordión</IonSelectOption>
                                <IonSelectOption value="acoustic_bass">Bajo</IonSelectOption>
                                <IonSelectOption value="alto_sax">Saxofón</IonSelectOption>
                                <IonSelectOption value="koto">Koto</IonSelectOption>
                            </IonSelect>   
                        </IonButton>
                        <IonContent className="piano-solo">
                            <div className="piano-solo-div-line"/>
                            <BasicPiano isPlayback={false} song={[]} instrumentName={this.state.soundfont}/>
                        </IonContent>
                    </IonContent>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default connect(mapStateToProps)(PianoSolo);
