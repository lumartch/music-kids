import React, { Component } from 'react';
import { IonPage,
    IonToolbar, 
    IonButtons,
    IonTitle, 
    IonContent,
    IonCard, 
    IonCardContent, IonButton } from '@ionic/react';
import './NotFound.css';
import { connect } from 'react-redux';
import  wrong  from '../../images/wrong.png'

class NotFound extends Component {
    render() {
        return (
            <IonPage>
                <IonContent className="not-found-background">
                    <IonCard className="not-found-card">
                        <IonToolbar className="not-found-toolbar">
                            <IonButtons>
                                <IonTitle color="white"className="not-found-title">... Oops, Algo salió mal</IonTitle>
                            </IonButtons>
                        </IonToolbar>
                        <IonCardContent>
                            <div className="not-found-div-container">
                                <div className="not-found-text">Tratamos de buscar la página... </div>
                                <img className="not-found-image" src={wrong} alt="NotFound"/>
                                <div className="not-found-text">Algo ocurrió mientras intentabas acceder a la página. </div>
                                <IonButton 
                                    className="not-found-button" 
                                    onClick={
                                        () => this.props.user_state.login 
                                        ? window.location.replace(`/home`) 
                                        : window.location.replace(`/login`)}
                                        >Regresar a Inicio
                                </IonButton>
                            </div>
                        </IonCardContent>
                    </IonCard>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
}

export default connect (mapStateToProps)(NotFound);