
export const SAVE_DATA_SUCCESS = 'SAVE_DATA_SUCCESS';
export const LOGOUT = 'LOGOUT';

export const fetchLogout = () => {
    return {
        type: LOGOUT
    }
}

export const saveData = (data) => {
    return {
        type: SAVE_DATA_SUCCESS,
        data: data
    }
}

export const logout = () => {
    return (dispatch) => {
        dispatch(fetchLogout())
    }
}

export const fetchLogin = (user) => {
    return (dispatch) => {
        dispatch(saveData(user))
    }
} 

export const updateData = (user) => {
    return (dispatch) => {
        dispatch(saveData(user))
    }
} 