export const UPDATE_PROGRESS = 'UPDATE_PROGRESS';
export const LOAD_PROGRESS = 'LOAD_PROGRESS';
export const CLEAR_PROGRESS = 'CLEAR_PROGRESS'

export const update = (field, theme) => {
    return {
        type: UPDATE_PROGRESS,
        field: field,
        theme: theme
    }
}

export const clear = () => {
    return {
        type: CLEAR_PROGRESS
    }
}

export const load = (data) => {
    return {
        type: LOAD_PROGRESS,
        data: data
    }
}

export const updateProgress = (field, theme) => {
    return (dispatch) => {
        dispatch(update(field, theme))
    }
}

export const loadProgress = (json) => {
    return (dispatch) => {
        dispatch(load(json))
    }
}

export const clearProgress = () => {
    return (dispatch) => {
        dispatch(clear())
    }
}
