import {LOAD_LESSONS, UPDATE_LESSON_FIELD, CLEAR_LESSONS} from '../actions/LessonsActions'

const default_lessons_state = {
    lessons: {}
}


const lessons_state = (state = default_lessons_state, action) => {
    switch(action.type) {
        case LOAD_LESSONS: 
            return {
                lessons: action.data
            }
        case UPDATE_LESSON_FIELD: 
            let json_lessons = state.lessons
            //json_lessons[action.field][action.theme]["Intentos"] =  json_lessons[action.field][action.theme]["Intentos"] + 1
            if(action.finished){
                if(json_lessons[action.field][action.theme]["Fallos"] > 0){
                    json_lessons[action.field][action.theme]["Fallos"] = json_lessons[action.field][action.theme]["Fallos"] - 1
                }  
            }
            else {
                json_lessons[action.field][action.theme]["Fallos"] = json_lessons[action.field][action.theme]["Fallos"] + 1
            }
            return {
                lessons: json_lessons
            }
        case CLEAR_LESSONS: 
            return {
                lessons: {}
            }
        default:
            return state
    }

}

export default lessons_state