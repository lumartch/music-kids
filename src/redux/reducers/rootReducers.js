import { combineReducers } from 'redux';
import user_state from "./LoginReducers";
import progress_state from "./ProgressReducer";
import songs_state from "./SongsReducers";
import lessons_state from "./LessonsReducers"

const rootReducers = combineReducers(
  { user_state, progress_state, songs_state, lessons_state  }
)

export default rootReducers;