import { LOAD_SONGS, CLEAR_SONGS, FINISHED_SONG, FAVORITE_SONG } from "../actions/SongsActions";

const default_songs_state = {
    songs: {},
    day_song: {},
    finished: 0,
    achievements: 0
}

const recommended_song = (songs) => {
    let day = new Date()
    // Línea para evitar BUGS el día 31, cosa la cual luego no será necesaria
    if (day.getDate() === 31 ){
        return { day:1 , song: songs[1] }
    } 
    return { day:day.getDate() , song: songs[day.getDate()] }
}

const finished_songs = (songs) => {
    let count = 0
    Object.entries(songs).map( (song) => {
        if(song[1]["finished"] === 1){
            count++
        }
        return null
    }) 
    return count
}

const achievements_songs = (songs) => {
    let count = 0
    Object.entries(songs).map( (song) => {
        if(song[1]["finished"] === 1){
            count++
        }
        return null
    }) 
    return count/5 === 0 ? 0 : Math.trunc(count/5 + 1);
}

const songs_state = (state = default_songs_state, action) => {
    switch (action.type) {
        case LOAD_SONGS: 
            let day_song = recommended_song(action.data)
            let finished = finished_songs(action.data)
            let achievements = achievements_songs(action.data)
            return {
                songs: action.data,
                day_song: day_song,
                finished: finished,
                achievements: achievements
            }
        
        case FINISHED_SONG:
            if(state.songs[action.id]["finished"] !== 1){
                let json_finished = state.songs
                json_finished[action.id]["finished"] = 1; 
                return {
                    songs: json_finished,
                    day_song: state.day_song,
                    finished: state.finished + 1,
                    achievements: achievements_songs(json_finished)
                }
            }
            return state

        case FAVORITE_SONG:
            let json_favorite = state.songs
            json_favorite[action.id]["favorite"] = action.value; 
            return {
                songs: json_favorite,
                day_song: state.day_song,
                finished: state.finished,
                achievements: state.achievements
            }

        case CLEAR_SONGS:
            return {
                songs: {},
                day_song: {},
                finished: 0,
                achievements: 0
            }

        default:
            return state
    }
}

export default songs_state;