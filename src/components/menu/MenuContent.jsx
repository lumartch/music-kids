import React, { Component } from 'react';
import { IonItem, IonLabel, IonIcon, IonMenuToggle }  from '@ionic/react';
import './MenuContent.css';

class MenuContent extends Component {
    constructor(){
        super()
        this.state = {
            title: "", 
            path: "", 
            icon: ""
        }
    }

    componentDidMount(){
        this.setState({
            title: this.props.title, 
            path: this.props.path,
            icon: this.props.icon
        })
    }

    render() {
        const { title, path, icon } = this.state 
        return (
            <IonMenuToggle>
                <IonItem className="menu-item" button key={ title } routerLink={ path } >
                    <IonLabel> <label className="menu-pages">{ title }</label></IonLabel>  
                    <IonIcon className="menu-icon" icon={icon}></IonIcon>
                </IonItem>
            </IonMenuToggle>
        );
    }
}

export default MenuContent;