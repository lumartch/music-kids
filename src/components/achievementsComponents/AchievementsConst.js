export const achievementsLessons = [
    [1, "Primeros pasos", "assets/icon/achievements/lessons/primeros pasos.png"],  
    [5, "Abejorro", "assets/icon/achievements/lessons/abejorro.png"],        
    [10, "Baby Mozart", "assets/icon/achievements/lessons/mozart.png"],    
    [15, "Concertista", "assets/icon/achievements/lessons/concertista.png"],    
    [20, "Joven Maestro", "assets/icon/achievements/lessons/maestro.png"],  
    [25, "Joven Beethoven", "assets/icon/achievements/lessons/beethoven.png"],
    [30, "Gran Bach", "assets/icon/achievements/lessons/bach.png"],      
    [35, "Piano man", "assets/icon/achievements/lessons/Piano Man.png"],      
    [40, "Músico :3", "assets/icon/achievements/lessons/musico.png"]       
];

export const achievementsSongs = [
    [1, "Deditos de bebé", "assets/icon/achievements/songs/baby_fingers.png"],     
    [10, "Dedines", "assets/icon/achievements/songs/dedines.png"],            
    [15, "Dedos ligeros", "assets/icon/achievements/songs/dedos_ligeros.png"],      
    [20, "Dedos de fuego", "assets/icon/achievements/songs/fire_fingers.png"],     
    [25, "¡Flash!", "assets/icon/achievements/songs/flash.png"],            
    [30, "¡Piano Hero!", "assets/icon/achievements/songs/piano hero.png"]        
]