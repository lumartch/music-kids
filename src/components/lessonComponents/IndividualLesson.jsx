import React, { Component } from 'react';
import { IonCheckbox, IonLabel, IonItem } from '@ionic/react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import './IndividualLesson.css'

class IndividualLesson extends Component {

    render() {
        const { lesson, isFinished } = this.props
        return (
            <div>
                {
                    this.props.disabled
                    ? <Link 
                        to={{
                            pathname:`/lessonscontent`,
                            state: {
                                lesson: this.props.lesson, 
                                header: this.props.header
                            }
                        }} 
                        className="link-lesson"
                    >
                        <IonItem className="ion-link" lines="none" disabled={ !this.props.disabled }>
                            <IonLabel> <label className="individual-name">{ lesson } </label></IonLabel>
                            <IonCheckbox 
                                mode="ios"
                                className="lesson-checked" 
                                slot="end"
                                color="primary" 
                                checked={ isFinished }/>   
                        </IonItem>
                    </Link>
                    :   <IonItem className="ion-link" lines="none" disabled={ !this.props.disabled }>
                            <IonLabel> <label className="individual-name">{ lesson } </label></IonLabel>
                            <IonCheckbox 
                                mode="ios"
                                className="lesson-checked" 
                                slot="end"
                                color="primary" 
                                checked={ isFinished }/>   
                        </IonItem>
                } 
            </div>
            
        );
    }
}

IndividualLesson.propTypes = {
    lesson: PropTypes.string.isRequired,
    isFinished: PropTypes.bool.isRequired,
}

export default withRouter(IndividualLesson);
