import React, { Component } from 'react';
import { connect } from 'react-redux';
import Lesson from './Lesson';
import store from '../../redux/store';
import {LessonsFetch} from './LessonsConst';
import { loadProgress } from '../../redux/actions/ProgressActions';
import  RecommendedLesson  from './RecommendedLesson';

class LessonsList extends Component {
    constructor(){
        super()
        this.state = {
            json: undefined,
        }
    }

    async componentDidMount(){
        try {
            let json = await LessonsFetch(this.props.user_state.user["correo"])
            store.dispatch(loadProgress(json))
        } catch(err) {
            let json = await fetch("assets/lessons-json/index-lessons.json").then(response => {
                let json = response.json()
                return json;
            })
            this.setState({
                json: json
            })
        }
    }

    _readJSON = (json) => {
        if(json === undefined){
            return
        }
        return Object.entries(json).map( (lessons, key) => {
            let content = []
            Object.entries(lessons[1]).map( (ls) => (
                content.push(ls)
            ))
            return <Lesson key={key} noLesson={lessons[0]} content={content}></Lesson>  
        })
    }

    render() {
        const { json } = this.state
        return (
            <div>
                {
                    this.props.progress_state.current.header !== "Sonidos" && 
                    this.props.progress_state.current.header !== "Presentación" &&
                    this.props.progress_state.current.header !== "Propiedades del sonido"
                    ? <RecommendedLesson/>
                    : null
                }
                {
                    json === undefined
                    ? this._readJSON(this.props.progress_state.progress)
                    : this._readJSON(json) 
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state,
        progress_state: state.progress_state,
        lessons: state.lessons_state.lessons
    }
  }


export default connect(mapStateToProps)(LessonsList);