import React, { Component } from 'react';
import { IonContent } from '@ionic/react';
import ClickNHold from 'react-click-n-hold'; 
import PianoKey from './PianoKey';
import "./PianoInterface.css"

var isMobile = false

class PianoInterface extends Component {
    _start(e, note){
        if(e.type === "touchstart") {
            isMobile = true
            this.props.appendNote([[note]])
            this.props.noteClicked(note)
        } else {
            if(e.type === "mousedown" && !isMobile){
                this.props.appendNote([[note]])
                this.props.noteClicked(note)
            }
            isMobile = false
        }
    }
    
	_end(){
        this.props.appendNote([[]]) 
    } 

    _generateKeys(indexNote, octave, key){
        return  <ClickNHold 
                    time={2} 
                    onStart={ (e) => this._start(e, indexNote + 48 + (12 * octave ))} 
                    onEnd={ () => this._end() } 
                    key={ key }
                >
                    <PianoKey indexNote={ indexNote } octave={ octave } disabled={this.props.disabled}/>      
                </ClickNHold>
    }

    _mapPiano = () => {
        let buttons = []
        for (let index = 0; index < 25; index++) {
            buttons.push( this._generateKeys((index%12), Math.trunc(index/12), index))
        }
        return buttons
    }

    render() {
        return (
            <IonContent className="piano-interface" >
                {
                    this._mapPiano()
                }
            </IonContent>
        );
    }
}

export default PianoInterface;