import React, {Component} from 'react';
import { IonItem, IonLabel, IonInput, IonButton, IonAlert } from '@ionic/react';
import { connect } from 'react-redux';
import {loginRequest} from "./LoginConst";
import { fetchLogin } from '../../redux/actions/LoginActions';
import store from '../../redux/store'
import { withRouter } from 'react-router';
import errorLogin from '../../images/noSongs.png';
import './Login.css';

class Login extends Component{
    constructor(){
        super()
        this.state = {
            inputEmail: "",
            inputPass:"",
            showAlert: false,
            msg: ""
        }
    }

    _handleEmailChange=(e)=>{
        this.setState({inputEmail: e.target.value})
    }

    _handlePassChange=(e)=>{
        this.setState({inputPass: e.target.value})
    }

    _handleSubmit  = async (e)  => {
        e.preventDefault()
        let response = await loginRequest(this.state.inputEmail, this.state.inputPass)
        if(response.alert === undefined){
            store.dispatch(fetchLogin(response))
            this.props.history.push("/home")
        } else {
            this.setState({ showAlert: response.alert, msg: this._generateHTML(response.msg, errorLogin)})
        }
    }

    _generateHTML = (myText, myImage) => {
        return (`
                    <label class="alert-label-login"> ${myText} </label> 
                    <img class="alert-img-login" alt="${myImage}" src=${myImage} /> 
                `)
    }
    
    render(){
        const { showAlert, msg } = this.state
        return (
            <form onSubmit={ this._handleSubmit }>
                <IonAlert
                    isOpen={ showAlert }
                    onDidDismiss={ () => this.setState({ showAlert: false }) }
                    backdropDismiss = { false }
                    cssClass = 'alert-login'
                    header = { "" }
                    subHeader = { "" }
                    message = { msg }
                    buttons = { ["Ok"] }
                />
                <IonItem className="background" lines="none">
                    <IonLabel className="label-login" position="floating"> Email </IonLabel>
                    <IonInput 
                    className="input-login"
                    required
                    type="email" 
                    placeholder="Ejemplo@gmail.com"
                    onIonChange={ this._handleEmailChange }> </IonInput>
                </IonItem>
                <IonItem className="background" lines = "none">
                    <IonLabel className="label-login" position="floating">Contraseña</IonLabel>
                    <IonInput
                    clearOnEdit="false"
                    className="input-login" 
                    required
                    type="password" 
                    placeholder="Password"
                    onIonChange={ this._handlePassChange }></IonInput>
                </IonItem>
                    <IonButton 
                    className="submit-button-login"
                    type="submit" 
                    expand="block"> Iniciar Sesión </IonButton>
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
  }

export default withRouter(connect(mapStateToProps)(Login));