import React, { Component } from 'react';
import { IonItem, IonInput, IonButton, IonLabel, IonAlert} from '@ionic/react';
import SecretQuestion from '../newAccount/SecretQuestion';
import updateData from '../../images/updateData.png';
//import noInternet from '../../images/wrong.png';
import './RecoverPassword.css';
import { secretQuestionFetch } from './RecoverSecretQuestion';
import { updatePasswordFetch } from './UpdatePassword';

let message = undefined;
let func = undefined;
class RecoverPassword extends Component{
    constructor() {
        super()
        this.state = {
            inputEmail: undefined,
            showError: undefined,
            messageError: undefined,
            showAlert: undefined,
            indexQuestion: undefined,
            alertMessage: undefined,
            inputPass: undefined,
            inputConfirm:undefined,
            inputAnswer:undefined
        }
        func = () => this.setState({ showAlert:false })
    }

    _handleEmailChange = (e) => {
        this.setState({ inputEmail: e.target.value })
    }
    _handleAnswerChange = (e) => {
        this.setState ({ inputAnswer: e.target.value })
    }
    _handleConfirmChange = (e) => {
        this.setState ({ inputConfirm: e.target.value })
    }
    _handlePassChange = (e)=>{ 
        this.setState ({ inputPass: e.target.value})
    }

    _handleSubmit = async (e) => {
        e.preventDefault()
        let response = await secretQuestionFetch(this.state.inputEmail)
        console.log("submit")
        if (response["invalidEmail"] === false){
            console.log(response["indexQuestion"])
            this.setState({
                indexQuestion:  parseInt(response["indexQuestion"]),
                showError: false
            })
        } else {
            this.setState({ 
                showError: true, 
                messageError: ( <div>
                                    Esta dirección de email no existe en la base de datos.
                                    <p>Por favor inténtalo de nuevo, o regístrate con una cuenta nueva.</p>
                                </div> )
            })
        }
    }

    _generateHTML = (myText, myImage) => {
        return (`
                    <label class="alert-label-recover"> ${myText} </label> 
                    <img class="alert-img-recover" alt="${myImage}" src=${myImage} /> 
                `)
    }

    _handleSubmitPass = async (e) =>{
        e.preventDefault()
        if(this.state.inputPass === this.state.inputConfirm  && this.state.inputAnswer !== undefined){
            let response = await updatePasswordFetch(this.state.inputEmail, this.state.inputAnswer, this.state.inputPass)
            if(response === "1"){
                message = this._generateHTML("Su contraseña ha sido actualizada.", updateData)
                func = () => window.location.replace("/login");
                this.setState({
                    showError: false,
                    showErrorAnswer:false,
                    showAlert: true
                })
            } else {
                this.setState({ 
                    showErrorAnswer: true
                })
            }
        }
        if(this.state.inputPass !== this.state.inputConfirm){
            this.setState({ 
                showError: true, 
                messageError: "Las contraseñas no coinciden, ingresalas nuevamente."
            })
        }

        // Cambiar el ShowError por uno especifico para el inputAnswer y le messageError
        if(this.state.inputAnswer === undefined){
            this.setState({
                showErrorAnswer: true
            })
        }
    }

    _renderSetNewPassword = () => {
        const { showError, messageError, showErrorAnswer } = this.state
        return(
            <form onSubmit={ this._handleSubmitPass }>
                <SecretQuestion question={ this.state.indexQuestion } disabled={true}/>
                <IonItem lines="none" className="recover-item">
                    { 
                        showErrorAnswer 
                        ?   <label className="recover-error-alert"> Ups, algo salió mal.
                        <p> Verifica que tu respuesta este bien escrita y vuelve a intentarlo.</p></label>
                        :   null 
                    }
                <IonLabel position= "floating">Escribe tu respuesta</IonLabel>
                    <IonInput 
                        className="recover-input"
                        required
                        placeholder="Respuesta Secreta"
                        onIonChange={ this._handleAnswerChange }>
                    </IonInput>
                </IonItem>
                <IonItem lines="none" className="recover-item">
                    { 
                        showError 
                        ?   <label className="recover-error-alert">{ messageError }</label>
                        :   null 
                    }
                    <IonLabel position= "floating">Escribe tu contraseña</IonLabel>
                    <IonInput 
                        className="recover-input"
                        required
                        type="password"
                        placeholder="Contraseña"
                        onIonChange={ this._handlePassChange }>
                    </IonInput>
                </IonItem>
                <IonItem lines="none" className="recover-item" >
                    <IonLabel position= "floating">Escribe tu contraseña</IonLabel>
                    <IonInput 
                        className="recover-input"
                        required
                        type="password"
                        placeholder="Confirmación de Contraseña"
                        onIonChange={ this._handleConfirmChange }>
                    </IonInput>
                </IonItem>
                <IonButton 
                    className="recover-button"
                    type="submitButton" 
                    expand="block">  
                    Cambiar contraseña
                </IonButton>
            </form>
        )
    }

    _renderForm = () => {
        const { showError, messageError } = this.state;
        return(
            <form onSubmit={ this._handleSubmit } disabled={true}>
                <IonItem lines="none" className="recover-item">
                    {   showError 
                        ?   <label className="recover-error-alert">{ messageError }</label>
                        :   null 
                    }
                    <IonLabel position= "floating">Escribe tu correo aqui</IonLabel>
                    <IonInput 
                        className="recover-input"
                        required
                        type="email"
                        placeholder="Ejemplo@gmail.com"
                        onIonChange={ this._handleEmailChange }>
                    </IonInput>
                </IonItem>
                <IonButton className="recover-button"
                    type="submitButton" 
                    expand="block">  
                    Verificar correo
                </IonButton>
            </form>
        )
    }

    render(){
        const { showAlert, indexQuestion } = this.state
        return(
            <div>
                <IonAlert
                        isOpen={ showAlert }
                        onDidDismiss={ func }
                        backdropDismiss = { false }
                        cssClass = 'alert-recover'
                        header = { "" }
                        subHeader = { "" }
                        message = { message }
                        buttons = { ["Ok"] }
                />
                {
                    indexQuestion !== undefined
                    ? this._renderSetNewPassword()
                    : this._renderForm()
                }
            </div>
        )
    }
};

export default RecoverPassword;