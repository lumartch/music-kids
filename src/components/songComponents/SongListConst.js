import { LINK_SERVER, IS_LOCAL } from "../../Const";

export const fetchSongs = (correo, search) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();   
        var url = IS_LOCAL ? LINK_SERVER + "/songs/?correo=" + correo + "&search=" + search : LINK_SERVER +  "/songs/getSongs.php";
        xhttp.open(IS_LOCAL ? "GET" : "POST", url, true);
        xhttp.onload = async () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                if(xhttp.status === 200){
                    resolve(JSON.parse(xhttp.responseText))
                } else {
                    let json = await fetch("assets/music-json/song-info.json").then(response => {
                        let json = response.json()
                        return json;
                    })
                    resolve(json)
                }
            } else {
                resolve(xhttp.statusText);
            }
        };
        let info = JSON.stringify({
            "correo": correo,
            "search": search
        })
        xhttp.send(IS_LOCAL ? null : info);
    })
}