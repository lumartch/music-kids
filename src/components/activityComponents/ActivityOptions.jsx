import React, { Component } from 'react';
import { IonGrid, IonRow, IonCol, IonButton } from '@ionic/react';
import './ActivityOptions.css';

let cssButtons = ["unselected","unselected","unselected","unselected"]

class ActivityOptions extends Component {
    constructor(){
        super()
        this.state = {
            option: "",
            active: true,
        }
    }

    componentDidMount(){
        cssButtons = ["unselected","unselected","unselected","unselected"]
    }

    _updateOption = (option, song) => {
        this.setState({
            option: option,
            active: false,
        })
        cssButtons = ["unselected","unselected","unselected","unselected"]
        cssButtons[option] = "selected"
        this.props.handleSong(song)
    }

    _checkAnswer = (answer) => {
        cssButtons = ["unselected","unselected","unselected","unselected"]
        this.props.handleAnswers(answer === this.state.option)
    }

    render() {
        const { arraySound, answer } = this.props
        const { active } = this.state
        return (
            <div>
                <IonGrid> 
                    <IonRow> 
                        <IonCol className="option-col">
                            <IonButton disabled={this.props.disabled}
                                className={"option-button right " + cssButtons[0]}
                                onClick={() => this._updateOption(0 , arraySound[0])}
                            >A</IonButton> 
                        </IonCol>
                        <IonCol className="option-col"> 
                            <IonButton disabled={this.props.disabled}
                                className={"option-button "+ cssButtons[1]}
                                onClick={() => this._updateOption(1 , arraySound[1])}
                            >B</IonButton> 
                        </IonCol>
                    </IonRow>
                    <IonRow> 
                        <IonCol className="option-col">
                            <IonButton disabled={this.props.disabled}
                                className={"option-button right " + cssButtons[2]}
                                onClick={() => this._updateOption(2 , arraySound[2])}
                            >C</IonButton>
                        </IonCol>
                        <IonCol className="option-col"> 
                            <IonButton disabled={this.props.disabled}
                                className={"option-button "+ cssButtons[3]}
                                onClick={() => this._updateOption(3 , arraySound[3])}
                            >D</IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <div className="card-content-options">
                    <IonButton
                        className="check-button"
                        disabled={ active }  
                        onClick={() => this._checkAnswer(answer) }>
                            Verificar
                    </IonButton>
                </div>
            </div>
        );
    } 
}

export default ActivityOptions;