import { LINK_SERVER, IS_LOCAL } from '../../Const';

export const emailFetch = (email) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        let url =  IS_LOCAL ? LINK_SERVER + "/account/?correo="+ email + "&recover=false" : LINK_SERVER + "/account/getEmail.php";
        xhttp.open(IS_LOCAL ? "GET" : "POST", url , true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                if (xhttp.responseText === "1"){
                    resolve( { invalidEmail: false })
                } else {
                    resolve({ 
                        invalidEmail: true, 
                        messageError: "Este correo ya existe, ingresa uno nuevo."
                    })
                }
            } else {
                resolve({ 
                    invalidEmail: true, 
                    messageError: "No se pudo efectuar la consulta a la base de datos."
                });
            }
        };
        xhttp.send( IS_LOCAL ? null : JSON.stringify({ "correo": email }))
    })
}