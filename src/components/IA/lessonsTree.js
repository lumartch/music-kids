export class Tree {
    constructor() {
        this.Nodes = []
    }

    buildTree(json){
        this.addNode(new Node("Actual", 0 , false, [])) 
        Object.entries(json).forEach((lessons, key) => {
            this.addNode(new Node(lessons[0], 0, false, []))
            Object.entries(lessons[1]).forEach((entry, key2) => {
                if (key === 0 ){//Caso para Sonidos
                    switch(key2){
                        case 0: 
                            this.addNode(new Node("Duración", 0, false, [])) 
                        break; 
    
                        case 2: 
                            this.addNode(new Node("Altura", 0, false, []))
                        break; 
    
                        case 4: 
                            this.addNode(new Node("Sonoridad", 0, false, []))
                        break; 

                        default: 

                        break; 
                    }
                }
                this.addNode(new Node(entry[0], entry[1]["Fallos"], true, null))
                
            })
        })

        this.Nodes[0].addChildren(this.Nodes[1]) //Sonidos
            this.Nodes[1].addChildren(this.Nodes[2]) //Duracion
                this.Nodes[2].addChildren(this.Nodes[3])
                this.Nodes[2].addChildren(this.Nodes[4])
    
            this.Nodes[1].addChildren(this.Nodes[5]) //Altura
                this.Nodes[5].addChildren(this.Nodes[6])
                this.Nodes[5].addChildren(this.Nodes[7])
    
            this.Nodes[1].addChildren(this.Nodes[8])//Sonoridad
                this.Nodes[8].addChildren(this.Nodes[9])
                this.Nodes[8].addChildren(this.Nodes[10])
                this.Nodes[8].addChildren(this.Nodes[11])
    
        this.Nodes[0].addChildren(this.Nodes[12])//Las Escalas
            this.Nodes[12].addChildren(this.Nodes[13])
            this.Nodes[12].addChildren(this.Nodes[14])
            this.Nodes[12].addChildren(this.Nodes[15])
            this.Nodes[12].addChildren(this.Nodes[16])
    
        this.Nodes[0].addChildren(this.Nodes[17])//Los Intervalos
            this.Nodes[17].addChildren(this.Nodes[18])
            this.Nodes[17].addChildren(this.Nodes[19])
            this.Nodes[17].addChildren(this.Nodes[20])
            this.Nodes[17].addChildren(this.Nodes[21])
            this.Nodes[17].addChildren(this.Nodes[22])
            this.Nodes[17].addChildren(this.Nodes[23])
            this.Nodes[17].addChildren(this.Nodes[24])
    
        this.Nodes[0].addChildren(this.Nodes[25])//Los acordes
            this.Nodes[25].addChildren(this.Nodes[26])
            this.Nodes[25].addChildren(this.Nodes[27])
            this.Nodes[25].addChildren(this.Nodes[28])
            this.Nodes[25].addChildren(this.Nodes[29]) 

        for(var i = 0; i < this.Nodes.length; i++){
            if(!this.Nodes[i].isLeaf){
                for(var j = 0; j < this.Nodes[i].children.length; j++){
                    this.Nodes[i].children[j].parent =  this.Nodes[i]
                }
            }  
        }
    }

    addNode(node) {
        this.Nodes.push(node)
    }

    updateTree() {
        this.calculateSum(this.Nodes[2]) //Duración 
        this.calculateSum(this.Nodes[5]) //Altura 
        this.calculateSum(this.Nodes[8]) //Sonoridad
        this.calculateSum(this.Nodes[1]) //Sonidos
        this.calculateSum(this.Nodes[12]) //Escalas 
        this.calculateSum(this.Nodes[17]) //Intervalos
        this.calculateSum(this.Nodes[25]) //Acordes
    }

    findCategory(lesson, root){
        var category = ""
        var node = lesson.parent
        while(node.label !== root.label){
            category = node.label
            node = node.parent
            if(node === null){
                return "default"
            }
        }
        return category
    }

    recommendLesson(relatedTopics){
        var relatedNodes = this.getListNodes(relatedTopics)
        var relation = [ 0, 0, 0 , 0 ]
        for(var i = 0; i < relatedNodes.length; i++){
            var category = this.findCategory(relatedNodes[i], this.Nodes[0])
            switch(category){
                case "Sonidos": 
                    relation[0] = relation[0] + 1 
                break; 
                
                case "Las escalas": 
                    relation[1] = relation[1] + 1
                break; 

                case "Los intervalos": 
                    relation[2] = relation[2] + 1
                break; 

                case "Los acordes": 
                    relation[3] = relation[3] + 1
                break; 

                default:

                break;
            }
        }
        var mostValuableIndex = 0; 
        var mostValuableSum = 0; 
        for(i = 0; i < this.Nodes[0].children.length; i++){ 
            if(this.Nodes[0].children[i].failures * (relation[i] + 1) > mostValuableSum){
                mostValuableSum = this.Nodes[0].children[i].failures * (relation[i] + 1)
                mostValuableIndex = i; 
            }
        }

        var secondLevelNode = this.Nodes[0].children[mostValuableIndex]
        
        if(secondLevelNode.label === "Sonidos") {
            var SecondRelation = [ 0, 0, 0]
            for(i = 0; i < relatedNodes.length; i++) {
                category = this.findCategory(relatedNodes[i], secondLevelNode)
                switch(category) {
                    case "Duración": 
                        SecondRelation[0] = SecondRelation[0] + 1 
                    break; 
                    
                    case "Altura": 
                        SecondRelation[1] = SecondRelation[1] + 1
                    break; 

                    case "Sonoridad": 
                        SecondRelation[2] = SecondRelation[2] + 1
                    break; 

                    default:

                    break;
                }
            }
            mostValuableIndex = 0; 
            mostValuableSum = 0; 
            for(i = 0; i < secondLevelNode.children.length; i++){ 
                if(secondLevelNode.children[i].failures * (SecondRelation[i] + 1) > mostValuableSum){
                    mostValuableSum = secondLevelNode.children[i].failures * (SecondRelation[i] + 1)
                    mostValuableIndex = i; 
                }
            }
            var thirdLevelNode = secondLevelNode.children[mostValuableIndex]
            mostValuableIndex = 0; 
            mostValuableSum = 0;
            let importance = 1
            for(i = 0; i < thirdLevelNode.children.length; i++){
                if(this.isRelated(thirdLevelNode.children[i], relatedNodes)){
                    importance = 2
                }

                if(thirdLevelNode.children[i].failures * importance > mostValuableSum){
                    mostValuableSum = thirdLevelNode.children[i].failures * (importance)
                    mostValuableIndex = i; 
                }
                importance = 1
            }
            return thirdLevelNode.children[mostValuableIndex]
        }
        else{
            mostValuableIndex = 0; 
            mostValuableSum = 0; 
            let importance = 1
            for(i = 0; i < secondLevelNode.children.length; i++){ 
                if(this.isRelated(secondLevelNode.children[i], relatedNodes)){
                    importance = 2
                }
                if(secondLevelNode.children[i].failures * importance > mostValuableSum){
                    mostValuableSum = secondLevelNode.children[i].failures * importance
                    mostValuableIndex = i; 
                }
                importance = 1
            }
            return secondLevelNode.children[mostValuableIndex]
        }
    }

    isRelated(lesson, list){
        for(var i = 0; i < list.length; i++){
            if(lesson.label === list[i].label){
                return true
            }
        }
        return false
    }

    getListNodes(relatedTopics){
        var listNodes = []
        for(var i = 0; i < relatedTopics.length; i++){
            for(var j = 0; j < this.Nodes.length; j++){
                if(relatedTopics[i] === this.Nodes[j].label){
                    listNodes.push(this.Nodes[j])
                    break
                }
            }
        }
        return listNodes; 
    }

    calculateSum(parentNode){
        var sum = 0
        for(var i = 0; i < parentNode.children.length; i++){
            sum = sum + parentNode.children[i].failures
        }
        parentNode.failures = sum  
    }

    
}

class Node {
    constructor(label, failures, isLeaf, children)
    {
        this.label = label
        this.failures = failures
        this.isLeaf = isLeaf
        this.children = children
        this.parent  = null  
    }

    addChildren(node){
        this.children.push(node)
    }
}
