import { LINK_SERVER,IS_LOCAL } from "../../Const";

export const IAFetch = (correo) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        var url = IS_LOCAL ? LINK_SERVER + "/ia/?correo=" + correo : LINK_SERVER +  "/ia/getIaJSON.php";
        xhttp.open(IS_LOCAL ? "GET" : "POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                resolve(JSON.parse(xhttp.responseText))
            } else {
                resolve(xhttp.responseText);
            }
        };
        var data = JSON.stringify({ "correo": correo });
        xhttp.send(IS_LOCAL ? null : data);
    })
}